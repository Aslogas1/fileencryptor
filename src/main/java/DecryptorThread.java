import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;

import java.io.File;

public class DecryptorThread extends Thread {

    private final GUIform form;
    private File file;
    private String password;

    public DecryptorThread(GUIform form) {
        this.form = form;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public void run() {
        onStart();
        try {
            String outPath = getOutputPath();
            ZipFile zipFile = new ZipFile(file);
            zipFile.setPassword(password);
            zipFile.extractAll(outPath);
            onFinish();
        } catch (ZipException e) {
            form.showWarning("Пароль введён неверно!");
        } catch (Exception e) {
            form.showWarning(e.getMessage());
        }
        form.setButtonsEnabled(true);

    }

    private void onStart() {
        form.setButtonsEnabled(false);
    }

    private void onFinish() {
        form.setButtonsEnabled(true);
        form.showFinished();
    }

    private String getOutputPath() {
        String path = file.getAbsolutePath()
                .replaceAll("\\.enc$", "");
        for (int i = 0; ; i++) {
            String number = i > 1 ? String.valueOf(i) : "";
            String outputPath = path + number;
            if (!new File(outputPath).exists()) {
                return outputPath;
            }
        }
    }
}
